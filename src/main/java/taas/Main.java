package taas;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner=new Scanner(System.in);
        System.out.print("your name ?");
        String name=scanner.nextLine();
        System.out.print("levels ?");
        int lv=scanner.nextInt();
        try
        {
            Thread game=new Game(lv,name);
            game.start();
        }catch(IllegalThreadStateException e){
            System.err.println(e.toString());
        }
    }
}
