package taas;

public class Game extends Thread {

    int lvl;
    String name;
    Game(int lv,String name){
        this.lvl=lv;
        this.name=name;
    }
    @Override
    public void run() {
        game(lvl, name);
    }

    int getTaas(){
        return (int) Math.ceil(Math.random()*6);
    }
    void game(int lv,String name) {
        int computer = 0, player = 0, same = 0;
        for (int i = 0; i < lv; i++) {

            int com = getTaas();
            int ply = getTaas();
            if (com > ply) {
                computer++;
            } else if (com < ply) {
                player++;
            } else {
                same++;
            }
            System.out.println(name + ": " + ply + "\tcomputer: " + com);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.err.println(name + ": " + player + "\ncomputer: " + computer + "\nsame result: " + same);
    }
}
