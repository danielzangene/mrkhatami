package stock;

public class StockHolder {
    private int id;
    private String name;
    private int budget;
    private Stock stock;

    public StockHolder(int id, String name, int budget, Stock stock) {
        this.id = id;
        this.name = name;
        this.budget = budget;
        this.stock = stock;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBudget() {
        return budget;
    }

    public Stock getStock() {
        return stock;
    }

    @Override
    public String toString() {
        return "\nStockHolder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", budget=" + budget +
                ", stock=" + stock +
                '}';
    }
}
