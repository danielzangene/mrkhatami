package stock;

public class Stock {
    private int id;
    private String title;
    private int price;
    private int count;

    public Stock(int id, String title, int price, int count) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.count = count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "\tStock{" +
                "\tid=" + id +
                "\t, title='" + title + '\'' +
                "\t, price=" + price +
                "\t, count=" + count +
                "\t}";
    }
}
