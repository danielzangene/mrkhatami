package stock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println("111");
        String[] stockTitle = new String[]{"gas", "petrol", "oil", "steel", "metal", "bronze", "silver"};
        String[] stockHolderNames = new String[]{"jack", "bill", "anton", "kurt", "rudy", "sonia", "connor"};

        List<StockHolder> stockHolders = new ArrayList<>();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < stockHolderNames.length; i++)
                stockHolders.add(new StockHolder(i, stockHolderNames[i], (int)( Math.random() * 100000),
                        new Stock(i, stockTitle[i], (int) (Math.random() * 10000), (int) (Math.random() * 100))));
        });
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.err.println(stockHolders.stream().filter(x -> x.getStock().getCount() * x.getStock().getPrice() > x.getBudget())
                .collect(Collectors.toList()));
//    calculateBudgets(stockHolders);
    }

    static void calculateBudgets(List<StockHolder> stockHolderList) {

        for (StockHolder stockHolder : stockHolderList) {
            List<StockHolder> legalStockHolders = Stream.of(stockHolder)
                    .filter(x -> (x.getStock().getCount() * x.getStock().getPrice()) < x.getBudget())
                    .collect(Collectors.toList());
            System.out.println(legalStockHolders);
        }
    }
}
