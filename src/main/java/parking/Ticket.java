package parking;

import java.awt.*;
import java.util.Date;

public class Ticket {
    private Date entranceTime;
    private String officename;
    private String carPlate;
    private Color color;
    private int budget;
    long finalReceipt;

    public Ticket(Date entranceTime, String officename, String carPlate, Color color, int budget) {
        this.entranceTime = entranceTime;
        this.officename = officename;
        this.carPlate = carPlate;
        this.color = color;
        this.budget = budget;
    }

    public void setFinalReceipt(long finalReceipt) {
        this.finalReceipt = finalReceipt;
    }

    public long getFinalReceipt() {
        return finalReceipt;
    }

    public Date getEntranceTime() {
        return entranceTime;
    }

    public String getOfficename() {
        return officename;
    }

    public String getCarPlate() {
        return carPlate;
    }

    public Color getColor() {
        return color;
    }

    public int getBudget() {
        return budget;
    }
}
