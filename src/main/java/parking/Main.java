package parking;

import java.awt.*;
import java.util.Date;

public class Main {
    final static long fee=2000;//per second in testing
    final static long standardTime=10;//per second

    public static void main(String[] args) {
        Ticket ticket=new Ticket(new Date(),"ali","58a5585-11", Color.BLACK,10000);
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ticket.setFinalReceipt(feeCalc(ticket));
        System.out.println("pay "+ticket.getFinalReceipt()+" Rial");
    }

    static long feeCalc(Ticket ticket){
        long timeStand=timeCalc(ticket);
        long paying=(timeStand*fee)-ticket.getBudget();
        if (timeStand>standardTime){
            return paying+5000;//penalty
        }
        return paying;
    }
    static long timeCalc(Ticket ticket){
        return ((new Date().getTime()-ticket.getEntranceTime().getTime())/1000);
    }

}
