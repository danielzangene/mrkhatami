import java.util.HashMap;
import java.util.Scanner;

public class BankNum {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("bank num ?");
        String bNum=scanner.nextLine();
        System.out.println(bankNum(bNum));
    }
    static String bankNum(String in){
        String[]str=in.split("-");
        if(validationBankNum(str)){
            String bankName=bankCodeNumbers().get(str[0]+"-"+str[1].substring(0,2));
            return bankName+"    "+str[0]+"-"+str[1]+"-"+str[2]+"-"+str[3];
        }
            return "bank num not valid!";

    }
    static boolean validationBankNum(String[]str){
        return str.length==4 &&
                str[0].length()==4&&
                str[1].length()==4&&
                str[2].length()==4&&
                str[3].length()==4&&
                bankCodeNumbers().containsKey(str[0]+"-"+str[1].substring(0,2));
    }
    static HashMap<String,String> bankCodeNumbers(){
        HashMap<String,String>map=new HashMap<>();
        map.put("2071-77",//2071-7725-2222-2222
                "saaderat");
        map.put("5022-29",
                "pasaargad");
        map.put("5028-06",//5028-0625-2525-5858
                "sharhr");
        map.put("5029-08",
                "taavon");
        map.put("5029-10",
                "kaarafarini");
        map.put("5029-38",
                "dey");
        map.put("5054-16",
                "gardeshgari");
        map.put("5057-85",
                "iranzamin");
        map.put("5058-01",
                "kosar");
        map.put("5892-10",
                "sepah");
        map.put("5894-63",
                "refah");
        map.put("6037-69",
                "saaderat");
        map.put("6037-70",
                "keshavarzi");
        map.put("6037-99",
                "melli");
        map.put("6063-73",
                "mehr");
        map.put("6104-33",
                "melat");
        map.put("6219-86",
                "saamaan");
        map.put("6221-06",
                "paarsian");
        map.put("6273-53",
                "tejarat");
        map.put("6273-81",
                "ansaar");
        map.put("6274-12",
                "eghtesadnovin");
        map.put("6274-88",
                "kaarafarini");
        map.put("6276-48",
                "saaderat");
        map.put("6277-60",
                "postbank");
        map.put("6278-84",
                "paarsian");
        map.put("6279-61",
                "sanatmadan");
        map.put("6280-23",
                "maskan");
        map.put("6281-57",
                "roseeh");
        map.put("6362-14",
                "taat");
        map.put("6367-95",
                "markazi");
        map.put("6369-49",
                "hekmat");
        map.put("6391-94",
                "paarsian");
        map.put("6392-17",
                "keshavarzi");
        map.put("6393-46",
                "sina");
        map.put("6393-47",
                "passargad");
        map.put("6393-70",
                "meheghtesad");
        map.put("6395-99",
                "ghavamin");
        map.put("6396-07",
                "sarmaye");
        map.put("9919-75",
                "melat");

        return map;
    }


}
